package com.wm.trade.api.dto.pay;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;


@Data
public class PosChargeBill implements Serializable {

    //分布式唯一Id将由snowFlake生成
    private Long id;

    /**
     * 商户编号
     */
    private String groupNo;

    /**
     * 收费单号
     */
    private String billId;

    /**
     * 客户号
     */
    private String customerCode;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 区域编号
     */
    private String regionNo;

    /**
     * 机构编号
     */
    private String orgNo;

    /**
     * POSID
     */
    private Integer posId;

    /**
     * 销售时间:精确到时间
     */
    private String makeDt;

    /**
     * 类型  0-正常;1-冲销 2-被冲销
     */
    private Integer type;

    /**
     * 原收费单号
     */
    private String sourceBillId;

    /**
     * 总金额(需做平等判断)
     */
    private BigDecimal totalAmt;

    /**
     * 收费类型ID
     */
    private Integer styleId;

    /**
     * 收银员ID
     */
    private Integer cashierId;

    /**
     * 收银员编号
     */
    private String cashierNo;

    /**
     * 收银员姓名
     */
    private String cashierName;

    /**
     * 第三方相关id
     */
    private String thirdPartyId;

    /**
     * 完成时间
     */
    private String completeTime;

    /**
     * 补录pos号
     */
    private String recordPosId;

    /**
     * 补录收银员号
     */
    private String recordCashierNo;

    /**
     * 会员信息
     */
    private String memberInfo;


    private static final long serialVersionUID = 1L;

}
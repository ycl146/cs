package com.wm.trade.api.interfaces.combine;

import com.wm.trade.core.model.Result;
import java.util.Map;

public interface CombineAPI {

    /**
     *合并订单
     * @return
     */
    Result combine(Map order);

}

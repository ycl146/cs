package com.wm.trade.api.dto.receipt;

import lombok.Data;

/**
 * @Auther: Huxinjia
 * @Date: 2019/3/5 17:38
 * @Description:
 */
@Data
public class Receipt {

    //小票号
    private String receiptNo;
    //销售类型
    private String saleType;
    //小票明细
    private String detail;
    //小票预览
    private String preview;
    //多点返回code
    private String code;
    //发送时间
    private String sendTime;
    //返回时间
    private String receiveTime;
    //返回消息
    private String returnMessage;
}

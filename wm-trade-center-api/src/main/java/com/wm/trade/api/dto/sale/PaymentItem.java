package com.wm.trade.api.dto.sale;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PaymentItem implements Serializable {

    //分布式唯一Id将由snowFlake生成
    private Long id;

    /**
     * sale表的id
     */
    private Long mainId;

    /**
     * 商户编号
     */
    private String groupNo;

    /**
     * 区域编号
     */
    private String regionNo;

    /**
     * 门店编号
     */
    private String orgNo;

    /**
     * pos号
     */
    private Integer posId;

    /**
     * 销售时间:精确到时间
     */
    private String saleDt;

    /**
     * 销售编号
     */
    private Integer saleId;

    /**
     * 支付类型编号
     */
    private Integer psId;

    /**
     * 支付号
     */
    private String paymentNum;

    /**
     * 支付金额(需做平等判断)
     */
    private BigDecimal paymentAmt;

    /**
     * 余额
     */
    private BigDecimal remainAmt;

    /**
     * 产生折扣的源支付方式
     */
    private Integer srcPsId;

    /**
     * 折扣名称
     */
    private String discountName;

    /**
     * 优惠金额
     */
    private BigDecimal discountAmt;

    /**
     * 税额
     */
    private BigDecimal invAmt;

    /**
     * 订单类型 1:销售订单 -1:退货单
     */
    private Integer saleType;

    private static final long serialVersionUID = 1L;

}
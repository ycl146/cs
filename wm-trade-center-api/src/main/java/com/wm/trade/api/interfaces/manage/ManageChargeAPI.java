package com.wm.trade.api.interfaces.manage;

import com.wm.trade.core.model.Result;

import java.util.Map;

public interface ManageChargeAPI {

    /**
     *查询收费列表
     * @return
     */
    Result searchChargeList(Map map);

    /**
     *查询收费详情
     * @return
     */
    Result searchChargeDetail(Map map);



}

package com.wm.trade.api.dto.sale;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SaleItem implements Serializable {

    //分布式唯一Id将由snowFlake生成
    private Long id;

    /**
     * sale表的id
     */
    private Long mainId;

    /**
     * 商家编号
     */
    private String groupNo;

    /**
     * 区域编号
     */
    private String regionNo;

    /**
     * 门店编号
     */
    private String orgNo;

    /**
     * pos号
     */
    private Integer posId;

    /**
     * 销售时间:精确到时间
     */
    private String saleDt;

    /**
     * 销售编号
     */
    private Integer saleId;

    /**
     * 明细编号
     */
    private Integer itemId;

    /**
     * 原销售表ID
     */
    private Integer sourceId;

    /**
     * 原销售明细表ID
     */
    private Integer sourceItemId;

    /**
     * 商品码(店内码)
     */
    private String sku;

    /**
     * 扫描码
     */
    private String scanPlu;

    /**
     * 商品名称
     */
    private String simpleName;

    /**
     * 销售数量:对于单品管理的商品，其意义与销售数量相同，对于金额管理的商品，其值为顾客对本商品的本次购买的重量（或容积等）
     */
    private BigDecimal saleQuantity;

    /**
     * 销售量:当顾客对任意一个商品发生一次购买时，销售数量增加一
     */
    private Integer saleCount;

    /**
     * 原售价
     */
    private BigDecimal normalPrice;

    /**
     * 系统售价:指后台进销存系统内存贮的价格，在没有可执行的促销计划时为一般零售价，否则为促销计划中的促销价格 
     */
    private BigDecimal retailPrice;

    /**
     * 会员价
     */
    private BigDecimal memberPrice;

    /**
     * 实际收款额:不含任何折扣(需做平等判断)
     */
    private BigDecimal saleAmt;

    /**
     * 不包括任何折扣的金额（实际金额减去条件促销后的金额）
     */
    private BigDecimal saleAmtBeforeDis;

    /**
     * 条件促销折扣金额
     */
    private BigDecimal discountAmt;

    private BigDecimal memNeedScore;

    /**
     * 条件促销档期号
     */
    private String promoPlanId;

    /**
     * 降价促销档期号
     */
    private String rpromoPlanId;

    /**
     * 条件促销号
     */
    private String promoId;

    /**
     * 降价促销号
     */
    private String rpromoId;

    /**
     * 商品分类编码
     */
    private String clsCode;

    /**
     * 商品课组编码
     */
    private String deptCode;

    /**
     * 1.自营标准商品 2.自营磅秤商品 3.自营大码 4.联营标志商品 5.联营称重商品 6.联营大码商品
     */
    private String merchType;

    /**
     * 商品类型1. 普通商品2. 散卖商品
     */
    private Integer dMerchType;

    /**
     * 出清类型1． 出清商品2． 正常商品
     */
    private Integer clearType;

    /**
     * 称重标识。0- 非磅秤商品1- 磅秤计数商品2- 磅秤称重商品
     */
    private Integer steelyardSaleType;

    /**
     * 称重位置 0:卖场称重, 1:银线称重
     */
    private Integer weightFlag;

    /**
     * 国条
     */
    private String barCode;

    /**
     * 入数
     */
    private BigDecimal includeSku;

    /**
     * 多点订单号
     */
    private String eorderId;

    /**
     * 多点子单号
     */
    private String eorderSubId;

    /**
     * wareCodeType 接口商品类型  商家内部的条码类型 如物美的枚举值如下： 0:未知类型; 1:国条码; 2:磅秤商品13码; 3:磅秤商品18码; 4:折扣标签16码; 5:物料编码; 6:2开头8位长度商品标码; 7:9位物料码
     */
    private Integer merchCodeType;

    /**
     * 商品输入方式 1-扫描 2-手工输入
     */
    private Integer merchInputStyle;

    /**
     * 商品输入时长(秒)
     */
    private Integer merchInputDur;

    /**
     * 收银员系统编号
     */
    private Integer cashierId;

    /**
     * 收银员工号
     */
    private String cashierNo;

    /**
     * 未税金额
     */
    private BigDecimal untaxedAmt;

    /**
     * 未税销价
     */
    private BigDecimal untaxedPrice;

    /**
     * 实际含税销售成本
     */
    private BigDecimal saleCost;

    /**
     * 发货含税销售不成本
     */
    private BigDecimal untaxedCost;

    /**
     * 订单类型 1:销售订单 -1:退货单
     */
    private Integer saleType;


    private static final long serialVersionUID = 1L;

}
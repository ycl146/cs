package com.wm.trade.api.dto.mongo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class OrderStatics implements Serializable {

    //由时间蹉+门店+posId（如2019021814_1019_236）
    private String id;

    //订单类型 0 收费 1 退货和销售
    private Integer type;

    //该小时内最后一次接收数据时间
    private String acceptDate;

    //该小时内最后一次推送kafka时间
    private String sendDate;

    //总金额
    private BigDecimal amount;

    //交易总数量
    private Long count;


}

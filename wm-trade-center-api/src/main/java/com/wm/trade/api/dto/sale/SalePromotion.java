package com.wm.trade.api.dto.sale;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SalePromotion implements Serializable {

    //分布式唯一Id将由snowFlake生成
    private Long id;

    /**
     * sale表的id
     */
    private Long mainId;

    /**
     * 商户编号
     */
    private String groupNo;

    /**
     * 区域编号
     */
    private String regionNo;

    /**
     * 门店编号
     */
    private String orgNo;

    /**
     * pos号
     */
    private Integer posId;

    /**
     * 销售时间:精确到时间
     */
    private String saleDt;

    /**
     * 销售编号
     */
    private Integer saleId;

    /**
     * 促销号
     */
    private Long promoId;

    /**
     * 促销简称
     */
    private String promoName;

    /**
     * 折扣金额
     */
    private BigDecimal discount;

    /**
     * 订单类型 1:销售订单 -1:退货单
     */
    private Integer saleType;

    private static final long serialVersionUID = 1L;

}
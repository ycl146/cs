package com.wm.trade.api.dto.receipt;

import lombok.Data;

/**
 * @Auther: Huxinjia
 * @Date: 2019/3/22 11:22
 * @Description:
 */
@Data
public class ReturnInfo {
    /**
     * 主键
     */
    private Long id;
    /**
     * 原订单groupNO
     */
    private String groupNo;
    /**
     * 原订单regionNo
     */
    private String regionNo;
    /**
     * 原订单orgNo
     */
    private String orgNo;
    /**
     * 原订单posId
     */
    private Integer posId;
    /**
     * 原订单saleId
     */
    private Integer saleId;
    /**
     * 原订单saleDt
     */
    private String saleDt;
    /**
     * 退货单PosId
     */
    private Integer rtnPosId;
    /**
     * 退货单SaleId
     */
    private Integer rtnId;
    /**
     * 退货单日期
     */
    private String rtnDt;
}

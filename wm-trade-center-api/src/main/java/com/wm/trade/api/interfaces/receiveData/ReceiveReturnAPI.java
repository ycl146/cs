package com.wm.trade.api.interfaces.receiveData;
import com.wm.trade.core.model.Result;
import java.util.Map;

public interface ReceiveReturnAPI {

    /**
     * 接收退货和销售订单
     * @return
     */
    Result receive(Map order,String acceptDate);

}

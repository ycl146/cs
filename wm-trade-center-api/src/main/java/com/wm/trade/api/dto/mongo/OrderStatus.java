package com.wm.trade.api.dto.mongo;

import lombok.Data;

@Data
public class OrderStatus {

    //分布式唯一Id
    private Long id;

    //订单类型 0 收费 1 退货和销售
    private Integer type;

    //接收时间
    private String acceptTime;

    //检核状态 1 成功
    private Integer checkStatus;

    //检核完成时间
    private String checkTime;

    //推送kafka状态 0初始，1成功，2失败
    private Integer kafkaStatus;

    //kafka修改时间
    private String  kafkaModifyTime;

    //存储sqlServer状态 0初始，1成功，2失败
    //注意:如果时收费则存sqlServer,如果时退货或销售则推kafka
    private Integer sqlServerStatus;

    //sqlServer修改时间
    private String  sqlServerModifyTime;

    //存储mongo状态 0初始，1成功，2失败
    private Integer mongoStatus;

    //mongo修改时间
    private String  mongoModifyTime;

    //推送多点状态 0 初始，1成功，2失败
    private Integer dMallStatus;

    //多点修改时间
    private String dMallModifyTime;

    //唯一标识
    //收费     groupNo_regionNo_orgNo_posId_billid_makeDt
    //销售退货 groupNo_regionNo_orgNo_posId_saleId_saleDt_saleType
    private String uniqueKey;
}

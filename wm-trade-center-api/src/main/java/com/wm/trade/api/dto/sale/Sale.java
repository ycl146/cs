package com.wm.trade.api.dto.sale;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class Sale implements Serializable {

    //分布式唯一Id将由snowFlake生成
    private Long id;
    /**
     * 商户编号
     */
    private String groupNo;

    /**
     * 区域编号
     */
    private String regionNo;

    /**
     * 门店编号
     */
    private String orgNo;

    /**
     * pos号
     */
    private Integer posId;


//    渠道100.云pos
//    101.老POS
//    102.OTO      (POSID 180)
//    103.全球精选 (POSID 181,182,183)
//    104.全球精选出清 (POSID 182)
//    105.多点单据退货 (POSID 184)
//    106.自由购   (POSID 190)
//    107.自助购  (POSID 193)
//    108.超嗨购物车 (POSID 194)
//    109.限制品加工 (POSID 120)
//    110.联营外卖  (POSID 121)
//    111.百货现金中心 (POSID 201)
//    112.移动POS (POSID 210-220)
//    113.团购

    private Integer source;

    /**
     * 销售时间:精确到秒
     */
    private String saleDt;

    /**
     * 订单类型 1:销售订单, -1:退货单
     */
    private Integer saleType;

    /**
     * 交易编号
     */
    private Integer saleId;

    /**
     * 实际收款总额:不含任何折扣(需做平等判断)
     */
    private BigDecimal totalAmt;

    /**
     * 条件促销总额
     */
    private BigDecimal totalDiscount;

    /**
     * 增减积分
     */
    private BigDecimal memNeedScore;

    /**
     * 会员电子卡号
     */
    private String memEcardNo;

    /**
     * 会员ID
     */
    private String memUserId;

    /**
     * 会员号
     */
    private String memCode;

    /**
     * 会员渠道
     */
    private String memCardChannel;

    /**
     * 扫描的会员号  （app，tel，card）
     */
    private String memInputCode;

    /**
     * 会员输入类型
     */
    private Integer memInputType;

    /**
     * 会员卡级别
     */
    private String memCardLevel;

    /**
     * 积分增减状态
     */
    private Integer memScoreFlag;

    /**
     * 发券 模板号
     */
    private String couponTempleNo;

    /**
     * 电商订单号
     */
    private String eorderId;

    /**
     * 电商订单状态
     */
    private String eorderStatus;

    /**
     * 主扫二维码
     */
    private String qrCode;

    /**
     * 多点商家ID
     */
    private String businessId;

    /**
     * 原销售的门店编号
     */
    private Integer sourceOrgNo;

    /**
     * 原销售的pos机编号
     */
    private Integer sourcePos;

    /**
     * 原销售日期时间
     */
    private String sourceSaleDt;

    /**
     * 原销售编号
     */
    private Integer sourceSaleId;

    /**
     * 退货原因编号
     */
    private Integer reasonId;

    /**
     * 商品扫描时长(秒)
     */
    private Integer merchInputDur;

    /**
     * 整单时长(秒)
     */
    private Integer transTotlDur;

    /**
     * 打印标志 0: 未打印 1: 已打印
     */
    private Integer printFlag;

    /**
     * 退货标志 0: 无退货 1: 有退货
     */
    private Integer rtnFlag;

    /**
     * 收银员类型 （1.正式工 2.小时工 3.支援  4.服务台 )
     */
    private Integer cashierType;

    /**
     * 收银员系统编号
     */
    private Integer cashierId;

    /**
     * 收银员工号
     */
    private String cashierNo;

    /**
     * 收银员姓名
     */
    private String cashierName;

    /**
     * 上传时间 
     */
    private String uploadDt;

    /**
     *  未税金额
     */
    private BigDecimal untaxedTotalAmt;

    /**
     *  客户号-团购专用
     */
    private String  customerCode;

    /**
     * 客户名称-团购专用
     */
    private String customerName;

    /**
     * 实际销售成本
     */
    private BigDecimal totCost;

    /**
     * 实际不含税销售成本
     */
    private BigDecimal untaxedTotCost;

    /**
     * 收货人
     */
    private String receivor;

    /**
     * 送货地址
     */
    private String address;

    /**
     * 邮编
     */
    private String postcode;

    /**
     * 移动电话
     */
    private String mobiltel;

    /**
     * 发货单号
     */
    private String shipmentId;

    /**
     * 发货日期
     */
    private String shipmentDt;

    /**
     * 发票号
     */
    private String billId;

    /**
     * 开票日期
     */
    private String billDt;


    private static final long serialVersionUID = 1L;

}
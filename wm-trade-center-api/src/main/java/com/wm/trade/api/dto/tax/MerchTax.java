package com.wm.trade.api.dto.tax;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MerchTax {

    //商品码(店内码)
    private String sku;

    //中分类编码
    private String mcCode;

    // 小分类编码
    private String emcCode;

    //税收分类编码
    private String taxCode;

    //税率
    private BigDecimal taxRate;

    //接收时间
    private Date receiveDt;
}

package com.wm.trade.api.dto.pay;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PosChargeBillItem implements Serializable {

    //分布式唯一Id将由snowFlake生成
    private Long id;

    /**
     * posChargeBill表的id
     */
    private Long mainId;

    /**
     * 商户编号
     */
    private String groupNo;

    /**
     * 收费单号
     */
    private String billId;

    /**
     * 明细ID
     */
    private String itemCode;

    /**
     * 收费项编码
     */
    private String pciCode;

    /**
     * 收费项名称
     */
    private String pciName;

    /**
     * 区域编号
     */
    private String regionNo;

    /**
     * 机构编号
     */
    private String orgNo;

    /**
     * POSID
     */
    private Integer posId;

    /**
     * 销售时间:精确到时间
     */
    private String makeDt;

    /**
     * 支付金额(需做平等判断)
     */
    private BigDecimal amount;

    /**
     * 卡编码
     */
    private String cardNo;

    /**
     * 手续费
     */
    private BigDecimal brokerage;

    private static final long serialVersionUID = 1L;

}
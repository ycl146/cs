package com.wm.trade.api.interfaces.receiveData;
import com.wm.trade.core.model.Result;
import java.util.Map;

public interface ReceivePayAPI {

    /**
     * 接收收费订单
     * @return
     */
    Result receive(Map order,String acceptDate);

}

package com.wm.trade.api.dto.receipt;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Auther: wangmz
 * @Date: 2019/3/4 11:19
 * @Description:
 */
@Data
public class OrderReceiptRelation {
    //主键
    private Long id;
    //商户编号
    private String groupNo;
    //区域编号
    private String regionNo;
    //门店
    private String orgNo;
    //pos号
    private Integer posId;
    //交易编号
    private Integer saleId;
    //交易类型
    private Integer saleType;
    //销售时间
    private String saleDt;
    //多点订单号
    private String eorderId;
}

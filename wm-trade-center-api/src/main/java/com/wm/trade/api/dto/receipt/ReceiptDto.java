package com.wm.trade.api.dto.receipt;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Auther: Huxinjia
 * @Date: 2019/3/2 11:19
 * @Description:
 */
@Data
public class ReceiptDto {

    //销售时间
    private String saleDt;
    //门店
    private String orgNo;
    //会员电子卡号
    private String ECardNo;
    //会员号
    private String MemCardNO;
    //
    private String memCardNoType;
    //
    private String memberCardType;
    //会员卡级别
    private String MemberCardLevel;
    //会员渠道
    private String MemberCardchannel;
    //小票金额
    private BigDecimal TotAmt;
}

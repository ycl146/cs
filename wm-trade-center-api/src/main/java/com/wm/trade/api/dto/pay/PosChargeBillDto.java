package com.wm.trade.api.dto.pay;

import lombok.Data;
import java.io.Serializable;
import java.util.List;

@Data
public class PosChargeBillDto  implements Serializable {
     //检核时间
    private String acceptDt;
    //收费主表
    private PosChargeBill posChargeBill;
    //收费明细表
    private List<PosChargeBillItem> posChargeBillItem;
    //收费支付表
    private List<PosChargeBillPayment> posChargeBillPayment;

}

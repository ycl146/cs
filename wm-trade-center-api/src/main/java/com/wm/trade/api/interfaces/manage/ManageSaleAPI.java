package com.wm.trade.api.interfaces.manage;

import com.wm.trade.core.model.Result;

import java.util.Map;

public interface ManageSaleAPI {

    /**
     *查询订单列表
     * @return
     */
    Result searchSaleList(Map map);

    /**
     *查询收费详情
     * @return
     */
    Result searchSaleDetail(Map map);



}

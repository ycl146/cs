package com.wm.trade.api.interfaces.resend;

import com.wm.trade.core.model.Result;

import java.util.Map;

public interface ResendAPI {

    /**
     * 重传指定订单
     * @return
     */
    Result resendSale(Map order);

    /**
     * 重传失败订单
     * @return
     */
    Result resendFail();

}

package com.wm.trade.api.dto.pay;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PosChargeBillPayment implements Serializable {

    //分布式唯一Id将由snowFlake生成
    private Long id;

    /**
     * posChargeBill表的id
     */
    private Long mainId;

    /**
     * 商户编号
     */
    private String groupNo;

    /**
     * 区域编号
     */
    private String regionNo;

    /**
     * 机构编号
     */
    private String orgNo;

    /**
     * POSID
     */
    private Integer posId;

    /**
     * 销售编号
     */
    private String billId;

    /**
     * 销售时间
     */
    private String makeDt;

    /**
     * 支付方式编号
     */
    private Integer psId;

    /**
     * 支付金额(需做平等判断)
     */
    private BigDecimal amount;

    private BigDecimal discountAmt;

    /**
     * 支付号
     */
    private String payNo;

    private static final long serialVersionUID = 1L;

}
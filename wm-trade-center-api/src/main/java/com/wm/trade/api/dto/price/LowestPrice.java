package com.wm.trade.api.dto.price;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class LowestPrice {

    //分布式唯一Id将由snowFlake生成
    private Long id;

    //商户编号
    private String groupNo;

    // 区域编号
    private String regionNo;

    //门店编号
    private String orgNo;

    //商品码(店内码)
    private String sku;

    //销售最低价
    private BigDecimal price;

    //日期
    private String date;
}

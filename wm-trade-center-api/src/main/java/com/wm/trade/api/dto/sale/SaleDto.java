package com.wm.trade.api.dto.sale;

import lombok.Data;
import java.io.Serializable;
import java.util.List;

@Data
public class SaleDto implements Serializable {
    //检核时间
    private String acceptDt;
    //是否是批量提交
    private Boolean isBatchSubmit;
    //销售订单
    private Sale sale;
    //销售订单明细
    private List<SaleItem> saleItem;
    //销售订单折扣商品明细
    private List<SalePromotion> salePromotion;
    //销售订单促销明细
    private List<SalePromotionItem> salePromotionItem;
    //支付主表
    private List<Payment> payment;
    //支付明细
    private List<PaymentItem> paymentItem;
    //支付折扣商品明细
    private List<PaymentDiscountItem> paymentDiscountItem;

}

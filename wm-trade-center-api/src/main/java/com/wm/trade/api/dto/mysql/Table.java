package com.wm.trade.api.dto.mysql;

import lombok.Data;

/**
 * @Auther: Huxinjia
 * @Date: 2019/3/7 10:18
 * @Description:
 */
@Data
public class Table {
    //表名
    private String tableName;
    //db名
    private String dbName;
}

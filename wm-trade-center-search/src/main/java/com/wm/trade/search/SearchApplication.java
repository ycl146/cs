package com.wm.trade.search;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubboConfiguration
public class SearchApplication {

    //etest
    public static void main(String[] args) {
        SpringApplication.run(SearchApplication.class, args);
    }

}

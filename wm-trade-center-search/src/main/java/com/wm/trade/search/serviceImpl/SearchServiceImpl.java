package com.wm.trade.search.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.wm.trade.api.dto.mongo.OrderStatics;
import com.wm.trade.api.dto.mongo.OrderStatus;
import com.wm.trade.api.interfaces.search.SearchAPI;
import com.wm.trade.core.model.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Service(interfaceClass = SearchAPI.class,filter = "tracing")
public class SearchServiceImpl implements SearchAPI{

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchServiceImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Result searchChargeBill() {
        Query query = new Query();
        List<OrderStatics> orderStatics = mongoTemplate.find(query, OrderStatics.class, "order_statics");
        return Result.ok(orderStatics);
    }


}

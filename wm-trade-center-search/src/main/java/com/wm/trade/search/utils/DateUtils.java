package com.wm.trade.search.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @Auther: Huxinjia
 * @Date: 2019/2/12 16:54
 * @Description:
 */
public class DateUtils {

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static final String DATE_TIME_FORMAT_YYYYMMDDHHMMSSSSS = "yyyy-MM-dd HH:mm:ss SSS";

    public static final String DATE_TIME_FORMAT_YYYYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_TIME_FORMAT_YYYYMMDD = "yyyyMMdd";

    public static String getCurrDateTime(){
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYYMMDDHHMMSS);
        String strDate = dtf.format(dateTime);

        return strDate;
    }

    public static String getCurrDateTimeSSS(){
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYYMMDDHHMMSSSSS);
        String strDate = dtf.format(dateTime);

        return strDate;
    }

    public static String getCurrDate(){
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYYMMDD);
        String strDate = dtf.format(dateTime);

        return strDate;
    }

    /*
     * 将时间转换为时间戳
     */
    public static String dateToStamp(String s) {

        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_FORMAT_YYYYMMDDHHMMSSSSS);
        Date date = null;
        try {
            date = simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long ts = date.getTime();
        res = String.valueOf(ts);

        return res;



    }

    /*
     * 将时间戳转换为时间
     */
    public static String stampToDate(String s){

        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_FORMAT_YYYYMMDDHHMMSSSSS);
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);

        return res;

    }

    /*
     * 当前时间 N天之前的时间
     */
    public static String getSubCurrDateTime(long days){

        LocalDateTime dateTime = LocalDateTime.now().minusDays(days);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYYMMDDHHMMSS);
        String strDate = dtf.format(dateTime);

        return strDate;
    }

    /*
     * 当前时间 N天N小时之前的时间
     */
    public static String getSubCurrDateTime(long days, long hours){

        LocalDateTime dateTime = LocalDateTime.now().minusDays(days).minusHours(hours);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYYMMDDHHMMSS);
        String strDate = dtf.format(dateTime);

        return strDate;
    }

    /*
     * 当前时间 N天N小时N分钟之前的时间
     */
    public static String getSubCurrDateTime(long days, long hours, long minutes){

        LocalDateTime dateTime = LocalDateTime.now().minusDays(days).minusHours(hours).minusMinutes(minutes);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYYMMDDHHMMSS);
        String strDate = dtf.format(dateTime);

        return strDate;
    }

    /*
     * 当前时间 N天之前的时间
     */
    public static String getAddCurrDateTime(long days){

        LocalDateTime dateTime = LocalDateTime.now().plusDays(days);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYYMMDDHHMMSS);
        String strDate = dtf.format(dateTime);

        return strDate;
    }

    /*
     * 当前时间 N天N小时之前的时间
     */
    public static String getAddCurrDateTime(long days, long hours){

        LocalDateTime dateTime = LocalDateTime.now().plusDays(days).plusHours(hours);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYYMMDDHHMMSS);
        String strDate = dtf.format(dateTime);

        return strDate;
    }

    /*
     * 当前时间 N天N小时N分钟之前的时间
     */
    public static String getAddCurrDateTime(long days, long hours, long minutes){

        LocalDateTime dateTime = LocalDateTime.now().plusDays(days).plusHours(hours).plusMinutes(minutes);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_YYYYMMDDHHMMSS);
        String strDate = dtf.format(dateTime);

        return strDate;
    }

    /*
     * 指定日期加上天数后的日期
     */
    public static String plusDay(int days,String date) {

        LocalDate dateTime = LocalDate.parse(date).plusDays(days);

        return dateTime.toString();
    }

}

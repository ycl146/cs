package com.wm.trade.core.utils;

import org.apache.commons.lang.StringUtils;

public class TradeStringUtil {

    /**
     * 如果对象为null或者空字符串则返回false
     * @param obj
     * @return
     */
    public static boolean isUseFulValue(Object obj){
        if(obj==null){
            return false;
        }else if(StringUtils.isBlank(obj.toString())||obj.toString().equals("null")){
            return false;
        }
        return true;
    }

    public static String fillZero(String str,int length){
        while(str.length()<length){
            str="0"+str;
        }
        return str;
    }

    public static String fillTime(String str){
        return str.replaceAll(" ","").replaceAll("-|:|T","").substring(2,14);
    }
}

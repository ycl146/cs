package com.wm.trade.core.exception;


public class SaveSqlServerException extends RuntimeException {
    public SaveSqlServerException() {
        super();
    }

    public SaveSqlServerException(Throwable cause) {
        super(cause);
    }
}

package com.wm.trade.core.model;

import lombok.Data;

@Data
public class ReceiveSuccess {

    private String id;

    public ReceiveSuccess(String id) {
        this.id = id;
    }
}

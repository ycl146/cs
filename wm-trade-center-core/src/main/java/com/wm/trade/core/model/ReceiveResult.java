package com.wm.trade.core.model;

import lombok.Data;

import java.util.List;

@Data
public class ReceiveResult {

    private List<ReceiveSuccess> successList;

    private List<ReceiveFail> failList;
}

package com.wm.trade.core.model;

import lombok.Data;

@Data
public class ReceiveFail {

    private String id;

    private String failReason;

    public ReceiveFail(String id, String failReason) {
        this.id = id;
        this.failReason = failReason;
    }
}

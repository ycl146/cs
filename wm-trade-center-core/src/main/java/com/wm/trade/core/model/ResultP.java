package com.wm.trade.core.model;


import lombok.Data;

@Data
public class ResultP<T> extends  Result<T>{

	/**
	 * 接口调用成功，有返回对象
	 */
	public static <T> ResultP<T> ok(T data,Long totalNum) {
		ResultP<T> result = new ResultP<>();
		result.setCode(200);
		result.setMsg("操作成功");
		result.setData(data);
		result.setTotalNum(totalNum);
		return result;
	}
	private Long totalNum;





}
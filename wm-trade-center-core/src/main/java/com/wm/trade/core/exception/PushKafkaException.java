package com.wm.trade.core.exception;

public class PushKafkaException extends RuntimeException {
    public PushKafkaException() {
        super();
    }

    public PushKafkaException(Throwable cause) {
        super(cause);
    }
}

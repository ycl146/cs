package com.wm.trade.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;


/**
 * 日期时间工具类
 * @since: 2018-4-11
 */
public class DateTimeUtils {

	/**
	 * 时间格式
	 */
	public enum TimeFormat {

		MMDD("MM.dd"),
		MMDDC("MM月dd日"),
		MM_DD("MM-dd"),
		HH_mm("HH:mm"),
		/**
		 * 短时间格式
		 */
		SHORT_DATE_PATTERN_LINE("yyyy-MM-dd"),
		SHORT_DATE_PATTERN_SLASH("yyyy/MM/dd"),
		SHORT_DATE_PATTERN_DOUBLE_SLASH("yyyy\\MM\\dd"),
		SHORT_DATE_PATTERN_NONE("yyyyMMdd"),

		/**
		 * 长时间格式
		 */
		LONG_DATE_PATTERN_MM("yyyy-MM-dd HH:mm"),
		LONG_DATE_PATTERN_LINE("yyyy-MM-dd HH:mm:ss"),
		LONG_DATE_PATTERN_SLASH("yyyy/MM/dd HH:mm:ss"),
		LONG_DATE_PATTERN_DOUBLE_SLASH("yyyy\\MM\\dd HH:mm:ss"),
		LONG_DATE_PATTERN_NONE("yyyyMMdd HH:mm:ss"),

		/**
		 * 长时间格式 带毫秒
		 */
		LONG_DATE_PATTERN_WITH_MILSEC_LINE("yyyy-MM-dd HH:mm:ss.SSS"),
		LONG_DATE_PATTERN_WITH_MILSEC_SLASH("yyyy/MM/dd HH:mm:ss.SSS"),
		LONG_DATE_PATTERN_WITH_MILSEC_DOUBLE_SLASH("yyyy\\MM\\dd HH:mm:ss.SSS"),
		LONG_DATE_PATTERN_WITH_MILSEC_NONE("yyyyMMdd HH:mm:ss.SSS");

		private transient DateTimeFormatter formatter;

		TimeFormat(String pattern) {
			formatter = DateTimeFormatter.ofPattern(pattern);
		}
	}


	/**
	 * 获取默认时间格式: yyyy-MM-dd HH:mm:ss
	 */
	private static final DateTimeFormatter DEFAULT_DATETIME_FORMATTER = TimeFormat.LONG_DATE_PATTERN_LINE.formatter;

	private DateTimeUtils() {
		// no construct function
	}

	/**
	 * String 转时间
	 *
	 * @param timeStr
	 * @return
	 */
	public static LocalDateTime parseTime(String timeStr) {
		return LocalDateTime.parse(timeStr, DEFAULT_DATETIME_FORMATTER);
	}

	/**
	 * String 转时间
	 *
	 * @param timeStr
	 * @param format  时间格式
	 * @return
	 */
	public static LocalDateTime parseTime(String timeStr, TimeFormat format) {
		return LocalDateTime.parse(timeStr, format.formatter);
	}

	/**
	 * 时间转 String
	 *
	 * @param time
	 * @return
	 */
	public static String parseTime(LocalDateTime time) {
		return DEFAULT_DATETIME_FORMATTER.format(time);
	}

	/**
	 * 时间转 String
	 *
	 * @param time
	 * @param format 时间格式
	 * @return
	 */
	public static String parseTime(Date date, TimeFormat format) {
		return format.formatter.format(dateToLocalDateTime(date));
	}


	public static LocalDateTime dateToLocalDateTime(Date date){
		Instant instant = date.toInstant();
		ZoneId zone = ZoneId.systemDefault();
		return LocalDateTime.ofInstant(instant, zone);
	}

	public static Date localDateTimeDateTo(LocalDateTime date){
		ZoneId zone = ZoneId.systemDefault();
		Instant instant = date.atZone(zone).toInstant();
		return Date.from(instant);
	}

	/**
	 * 获取当前时间
	 *
	 * @return
	 */
	public static String getCurrentDatetime() {
		return DEFAULT_DATETIME_FORMATTER.format(LocalDateTime.now());
	}

	/**
	 * 获取当前时间
	 *
	 * @param format 时间格式
	 * @return
	 */
	public static String getCurrentDatetime(TimeFormat format) {
		return format.formatter.format(LocalDateTime.now());
	}


	/**
	 * 获取指定星期的第一天(周一)
	 *
	 * @return
	 */
	public static Date getFirstDayOfCurrentWeek(Date date) {
		LocalDateTime localDateTime = dateToLocalDateTime(date);
		LocalDateTime with = localDateTime.with(DayOfWeek.MONDAY);
		return localDateTimeDateTo(with);
	}
	/**
	 * 获取指定星期的第一天(周日)
	 *
	 * @return
	 */
	public static Date getLastDayOfCurrentWeek(Date date) {
		LocalDateTime localDateTime = dateToLocalDateTime(date);
		LocalDateTime with = localDateTime.with(DayOfWeek.SUNDAY);
		return localDateTimeDateTo(with);
	}

	public static String dateToStringEmpty(Date date) {
		if(date==null){
			return "空";
		}
		return parseTime(date, TimeFormat.SHORT_DATE_PATTERN_LINE);
	}

	public static String dateToStringMMDD(Date date) {
		if(date==null){
			return "";
		}
		return parseTime(date, TimeFormat.MMDD);
	}

	public static String dateToStringMMDDC(Date date) {
		if(date==null){
			return "";
		}
		return parseTime(date, TimeFormat.MMDDC);
	}

	public final static String sdfyyyy = "yyyy";

	public final static String sdfyyyyMM = "yyyy-MM";

	public final static String sdfyyyyMMdd = "yyyy-MM-dd";

	public final static String sdfyyyyMMddHH = "yyyy-MM-dd HH";

	public final static String sdfyyyyMMddHHmm = "yyyy-MM-dd HH:mm";

	public final static String sdfyyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";

	public final static String sdfMM = "MM";

	public final static String sdfdd = "dd";

	public final static String sdfHH = "HH";

	public final static String sdfmm = "mm";

	public final static String sdfss = "ss";

	public final static String sdfHHmm = "HH:mm";

	public final static String sdfHHmmss = "HH:mm:ss";

	/**
	 * 是否是同一天
	 * @param date1
	 * @param date2
	 * @return
	 * @throws ParseException 
	 */
	public static boolean isSameDate(Date date1, Date date2) throws ParseException {
		return toDayStart(date1).getTime() == toDayStart(date2).getTime();
	}

	/**
	 * @description: 比较date1是否在date2之前
	 * @param date1
	 * @param date2
	 * @return
	 * @throws ParseException 
	 */
	public static boolean isBefore(Date date1, Date date2) throws ParseException {
		return toDayStart(date1).getTime() <= toDayStart(date2).getTime();
	}



	

	/**
	 * 指定日期的开始时间: 2010-10-10 00:00:00
	 * 
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	public static Date toDayStart(Date date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(sdfyyyyMMdd);
		if(null != date){
			return sdf.parse(sdf.format(date));
		}else{
			return null;
		}
	}

	/**
	 * 指定日期的最后时间: 2010-10-10 23:59:59
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	public static Date toDayEnd(Date date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(sdfyyyyMMdd);
		Date tmp = null;
		if(null != date){
			tmp = sdf.parse(sdf.format(date));
		}else{
			return null;
		}
		return new Date(tmp.getTime() + 24 * 60 * 60 * 1000 - 1);
	}


	












	/**
	 * 获取当前日期是星期几
	 * 
	 * @return
	 */
	public static int getWeek() {
		return getWeek(new Date());
	}

	/**
	 * 获取指定日期是星期几
	 * 
	 * @return
	 */
	public static int getWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
		return week < 0 ? 0 : week;
	}







	/**
	 * 获取两个日期之间相差几天
	 * @param date1
	 * @param date2
	 * @return
	 * @throws ParseException 
	 */
	public static int getDiffDays(Date date1, Date date2) throws ParseException {
		long diffTimes = Math.abs(toDayStart(date2).getTime() - toDayStart(date1).getTime());
		return (int) (diffTimes / (1000 * 60 * 60 * 24));
	}

	/**
	 * 获取当前月份的第一天
	 * @return
	 * @throws ParseException 
	 */
	public static Date getFirstDayOfCurrentMonth() throws ParseException {
		return getFirstDayOfCurrentMonth(new Date());
	}

	/**
	 * 获取指定月份的第一天
	 * @return
	 * @throws ParseException 
	 */
	public static Date getFirstDayOfCurrentMonth(Date date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(sdfyyyyMM);
		if(null != date) {
			return sdf.parse(sdf.format(date));
		} else{
			return null;
		}
	}












	/**
	 * @description: 获得向前或者向后算的日期
	 * @modifier:
	 * @modifiedDate:
	 * @param i 负数表示向前算多少天，正数表示往后多少天
	 * @return
	 */
	public static Date getDateBySpecify(int i) {
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, i);
		return now.getTime();
	}


	/**
	 * @description: 返回指定的时间向前或者向后几天的日期
	 * @modifier:
	 * @modifiedDate:
	 * @param date 指定的时间
	 * @param i 负数表示向前算多少天，正数表示往后多少天
	 * @return
	 */
	public static Date getDateBySpecify(Date date, int i) {
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date);
		rightNow.add(Calendar.DATE, i);
		return rightNow.getTime();
	}




	/**
	 * 
	 * @description: 获取当前的日期
	 * @createDate: 2016年10月11日
	 * @modifier:
	 * @modifiedDate:
	 * @param pattern
	 * @return
	 */
	public static String currentDate(String pattern) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String returnTime;
		Date currentTime = new Date();
		returnTime = formatter.format(currentTime);
		return returnTime;
	}

	/**
	 * 
	 * @description: 获取指定日期与当前时间差   如果是同一天 返回时差   如果不同天  则返回日期
	 * @creator: hsh.chen
	 * @createDate: 2016年10月11日 
	 * @modifier:
	 * @param strDate  传入的格式是 yyyy-MM-dd HH:mm:ss
	 * @modifiedDate:
	 * @return
	 */
	public static String currentTimeDiff(String strDate) {
		String returnStr = null;
		String currentDate = currentDate(sdfyyyyMMddHHmmss);//当前日期
		if(!strDate.isEmpty()){
			if (strDate.substring(0, 11).equals(currentDate.substring(0, 11))) {//比较时间	
				int currentHour = Integer.parseInt(currentDate.substring(11, 13));
				int strDateHour = Integer.parseInt(strDate.substring(11, 13));
				if (currentHour == strDateHour) {//小时相等 返回分钟
					int currentSec = Integer.parseInt(currentDate.substring(14, 16));
					int strDateSec = Integer.parseInt(strDate.substring(14, 16));

					if (currentSec == strDateSec) {//分钟相等 返回秒
						int currentMin = Integer.parseInt(currentDate.substring(17, 19));
						int strDateMin = Integer.parseInt(strDate.substring(17, 19));
						return (currentMin - strDateMin) + "秒前";
					} else {
						return (currentSec - strDateSec) + "分钟前";
					}

				} else {
					return (currentHour - strDateHour) + " 小时前";
				}

			} else {
				returnStr = strDate.substring(0, 11);
			}
		}
		return returnStr;

	}



	/**
	 * 获取当前时间
	 * @return
	 */
	public static Date getNow(){
		ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.now();
		ZonedDateTime zdt = localDateTime.atZone(zoneId);

		return Date.from(zdt.toInstant());
	}

	/**
	 * 时间戳转换成日期格式字符串
	 * @param seconds 精确到秒的字符串
	 * @param format
	 * @return
	 */
	public static String timeStamp2DateString(String seconds,String format) {
		if(seconds == null || seconds.isEmpty() || seconds.equals("null")){
			return "";
		}
		if(format == null || format.isEmpty()){
			format = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(new Date(Long.valueOf(seconds)));
	}


	/**
	 * 时间戳转换成日期格式字符串
	 * @param seconds 精确到秒的字符串
	 * @param format
	 * @return
	 */
	public static Date timeStamp2Date(String seconds,String format) {
		if(seconds == null || seconds.isEmpty() || seconds.equals("null")){
			return null;
		}
		if(format == null || format.isEmpty()){
			format = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateString = sdf.format(new Date(Long.valueOf(seconds)));
		Date date=null;
		try {
			 date = sdf.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}


	/**
	 * 日期格式字符串转换成时间戳
	 * @param date_str 字符串日期
	 * @param format 如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String date2TimeStamp(String date_str,String format){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return String.valueOf(sdf.parse(date_str).getTime()/1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 取得当前时间戳（精确到秒）
	 * @return
	 */
	public static String timeStamp(){
		long time = System.currentTimeMillis();
		String t = String.valueOf(time/1000);
		return t;
	}

	/**
	 * java.util.Date --> java.time.LocalTime
	 * @return
	 */
	public static LocalTime UDateToLocalTime(Date date) {
		Instant instant = date.toInstant();
		ZoneId zone = ZoneId.systemDefault();
		LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
		return localDateTime.toLocalTime();
	}

	public static Boolean compareHourMinute(Date startDate,Date endDate){
		int st = getHourMinute(startDate);
		int et = getHourMinute(endDate);
		int now = getHourMinute(DateTimeUtils.getNow());
		if( st < now && et > now ){
			return true;
		} else {
			return false;
		}
	}

	public  static int getHourMinute(Date date){
		LocalTime localTime = UDateToLocalTime(date);
		return localTime.getHour() * 100 + localTime.getMinute();
	}

	/**
	 * 日期转时间戳
	 * @param date
	 * @return
	 */
	public static Integer transForMilliSecond(Date date){
		if(date==null) return null;
		return (int)(date.getTime()/1000);
	}

	/**
	 * 过去几天的时间
	 * @param days
	 * @return 格式化好的时间字符串 例如：2018-05-20 17:26:21
	 */
	public static LocalDateTime previousDays(Integer days){
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime previous = now.minusDays(days);
		return previous;
	}

	/**
	 * 获得某天最大时间 2017-10-15 23:59:59
	 * @param time
	 * @return
	 */
	public static String getEndOfDay(LocalDateTime time) {
		LocalDateTime endOfDay = time.with(LocalTime.MAX);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(sdfyyyyMMddHHmmss);
		return formatter.format(endOfDay);
	}

	/**
	 * 获得某天最小时间 2017-10-15 00:00:00
	 * @return
	 */
	public static String getStartOfDay(LocalDateTime time) {
		LocalDateTime startOfDay = time.with(LocalTime.MIN);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(sdfyyyyMMddHHmmss);
		return formatter.format(startOfDay);

	}

	/**
	 * 传入 2017-10-15 23:59:59 转为 2017-10-15
	 * @param time
	 * @return
	 */
	public static String convertTimeToDate(String time){
		SimpleDateFormat sdf=new SimpleDateFormat(sdfyyyyMMdd);
		Date date= null;
		try {
			date = sdf.parse(time);
			return sdf.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}

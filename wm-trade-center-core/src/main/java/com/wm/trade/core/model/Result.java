package com.wm.trade.core.model;

import com.alibaba.fastjson.JSON;

public class Result<T> implements  java.io.Serializable{
	/**
	 * 接口调用成功，不需要返回对象
	 */
	public static <T> Result<T> ok(){
		Result<T> result = new Result<>();
		result.setCode(200);
		result.setMsg("操作成功");
		return result;
	}

	/**
	 * 接口调用成功，有返回对象
	 */
	public static <T> Result<T> ok(T data) {
		Result<T> result = new Result<>();
		result.setCode(200);
		result.setMsg("操作成功");
		result.setData(data);
		return result;
	}

	public static <T> Result<T> ok(int code,T data) {
		Result<T> result = new Result<>();
		result.setCode(code);
		result.setMsg("操作成功");
		result.setData(data);
		return result;
	}

	/**
	 * 接口调用失败，有错误码和描述，没有返回对象
	 */
	public static Result fail(int code, String msg){
		Result result = new Result<>();
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

	private int code;
	private T data;
	private String msg;

	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String toString() {
		if(null==data){
			return  "{\"code\":" + code + ",\"msg\":" + msg + "}";
		}else {
			String dataStr=JSON.toJSONString(data);
			return "{\"code\":" + code + ",\"msg\":" + msg + ",\"data\":" + dataStr + "}";
		}
	}

}
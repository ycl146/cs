package com.wm.trade.core.exception;


public class SaveMongoException extends RuntimeException {
    public SaveMongoException() {
        super();
    }

    public SaveMongoException(Throwable cause) {
        super(cause);
    }
}

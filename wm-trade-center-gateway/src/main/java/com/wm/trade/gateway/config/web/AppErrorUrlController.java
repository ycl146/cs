package com.wm.trade.gateway.config.web;

import com.wm.trade.core.enums.ResultStatus;
import com.wm.trade.core.model.Result;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AppErrorUrlController implements ErrorController {

    @RequestMapping(value = "/error")
    public Result error() {
        return Result.fail(ResultStatus.error_invalid_argument.getErrorCode(),ResultStatus.error_invalid_argument.getErrorMsg());
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}

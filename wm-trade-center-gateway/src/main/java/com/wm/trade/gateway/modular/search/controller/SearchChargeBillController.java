package com.wm.trade.gateway.modular.search.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.wm.trade.api.interfaces.search.SearchAPI;
import com.wm.trade.core.enums.ResultStatus;
import com.wm.trade.core.model.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class SearchChargeBillController {

    @Reference(interfaceClass = SearchAPI.class, check = false, filter = "tracing")
    private SearchAPI searchAPI;

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchChargeBillController.class);

    @GetMapping(value = "/test")
    public Result search() {
        try {
            Result result= searchAPI.searchChargeBill();
            return result;
        } catch (Exception e) {
            return Result.fail(ResultStatus.http_status_internal_server_error.getErrorCode(),ResultStatus.http_status_internal_server_error.getErrorMsg());
        }
    }
}
